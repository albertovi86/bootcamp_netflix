package com.example.bootcamp.repository;

import com.example.bootcamp.entity.Chapter;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ChapterRepository extends JpaRepository<Chapter, Integer> {
    List<Chapter> findBySeasonIdAndNameEndingWith(Long seasonId, String suffix);

    @Query("SELECT c FROM Chapter c WHERE c.season.id = :seasonId AND c.name LIKE %:suffix")
    List<Chapter> findChaptersBySeasonAndNameEndingWith(@Param("seasonId") int seasonId, @Param("suffix") String suffix);

}
