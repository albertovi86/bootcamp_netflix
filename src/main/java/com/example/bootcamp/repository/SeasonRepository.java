package com.example.bootcamp.repository;

import com.example.bootcamp.entity.Season;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository

public interface SeasonRepository extends JpaRepository<Season, Integer> {

}
