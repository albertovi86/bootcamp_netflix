package com.example.bootcamp.repository;

import com.example.bootcamp.entity.Actor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ActorRepository extends JpaRepository<Actor, Integer> {
    default List<Actor> findAllByOrderByNombreAsc() {
        return null;
    }
    List<Actor> findAllByNameStartingWithIgnoreCase(String prefix);
}
