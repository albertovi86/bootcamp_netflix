package com.example.bootcamp.repository;

import com.example.bootcamp.entity.TvShow;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface TvShowRepository extends JpaRepository<TvShow, Integer> {
    @Query("SELECT t FROM TvShow t JOIN FETCH t.category")
    List<TvShow> findAllWithCategory();
    List<TvShow> findAll(Sort sort);
}



