package com.example.bootcamp.repository;

import com.example.bootcamp.entity.Album;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
public interface AlbumRepository extends JpaRepository<Album, Integer> {
    @Query("SELECT a FROM Album a ORDER BY a.yearRelease ASC")
    List<Album> findAllSortedByYearRelease();
}
