package com.example.bootcamp.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import javax.persistence.*;
import java.util.Collection;

@Entity
@Table(name = "tv_shows")
public class TvShow {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String name;

    @Column(name = "short_description")
    private String shortDescription;

    @Column(name = "long_description")
    private String longDescription;
    private int year;

    @Column(name = "recommended_age")
    private int recommendedAge;
    private String advertising;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "category_id")
    private Category category;

    @Transient
    @JsonProperty
    private double rate;
    public TvShow() {
    }
    public TvShow(String name, String shortDescription, String longDescription, int year, int recommendedAge, String advertising, Category category) {
        this.name = name;
        this.shortDescription = shortDescription;
        this.longDescription = longDescription;
        this.year = year;
        this.recommendedAge = recommendedAge;
        this.advertising = advertising;
        this.category = category;
    }

    // Getters y setters

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getShortDescription() {
        return shortDescription;
    }
    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }
    public String getLongDescription() {
        return longDescription;
    }
    public void setLongDescription(String longDescription) {
        this.longDescription = longDescription;
    }
    public int getYear() {
        return year;
    }
    public void setYear(int year) {
        this.year = year;
    }
    public int getRecommendedAge() {
        return recommendedAge;
    }
    public void setRecommendedAge(int recommendedAge) {
        this.recommendedAge = recommendedAge;
    }
    public String getAdvertising() {
        return advertising;
    }
    public void setAdvertising(String advertising) {
        this.advertising = advertising;
    }
    public Category getCategory() {
        return category;
    }
    public void setCategory(Category category) {
        this.category = category;
    }
    public void addSeason(Season existingSeason) {
    }
    public void removeSeason(Season existingSeason) {
    }
    public void addCategory(Category existingCategory) {
    }
    public void removeCategory(Category existingCategory) {
    }
    public Collection<? extends Chapter> getChapters() {
        return null;
    }
    public void setRate(double rate) {
        this.rate = rate;
    }
}

