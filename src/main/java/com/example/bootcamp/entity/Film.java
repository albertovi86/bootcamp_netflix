package com.example.bootcamp.entity;

public class Film {
    private double rate;

    public Film() {
    }

    public Film(double rate) {
        this.rate = rate;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }
}
