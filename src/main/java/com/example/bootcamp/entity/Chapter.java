package com.example.bootcamp.entity;

import javax.persistence.*;

@Entity
@Table(name = "chapters")
public class Chapter {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private int number;

    private String name;

    private int duration;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "season_id")
    private Season season;
    public Chapter() {
    }
    public Chapter(int number, String name, int duration, Season season) {
        this.number = number;
        this.name = name;
        this.duration = duration;
        this.season = season;
    }

    // Getters y setters
    public int getId() {
        return Math.toIntExact(id);
    }
    public void setId(int id) {
        this.id = (int) id;
    }
    public int getNumber() {
        return number;
    }
    public void setNumber(int number) {
        this.number = number;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public int getDuration() {
        return duration;
    }
    public void setDuration(int duration) {
        this.duration = duration;
    }
    public Season getSeason() {
        return season;
    }
    public void setSeason(Season season) {
        this.season = season;
    }
    public void addActor(Actor existingActor) {
    }
    public void removeActor(Actor existingActor) {
    }


}


