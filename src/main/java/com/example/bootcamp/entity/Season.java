package com.example.bootcamp.entity;

import javax.persistence.*;

@Entity
@Table(name = "seasons")
public class Season {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private int number;

    private String name;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "tv_show_id")
    private TvShow tvShow;

    public Season() {
    }
    public Season(int number, String name, TvShow tvShow) {
        this.number = number;
        this.name = name;
        this.tvShow = tvShow;
    }

    // Getters y setters
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public int getNumber() {
        return number;
    }
    public void setNumber(int number) {
        this.number = number;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public TvShow getTvShow() {
        return tvShow;
    }
    public void setTvShow(TvShow tvShow) {
        this.tvShow = tvShow;
    }
    public void addChapter(Chapter existingChapter) {
    }
    public void removeChapter(Chapter existingChapter) {
    }
}


