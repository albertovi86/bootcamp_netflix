package com.example.bootcamp.controller;

import com.example.bootcamp.entity.Category;
import com.example.bootcamp.entity.Film;
import com.example.bootcamp.entity.TvShow;
import com.example.bootcamp.service.CategoryService;
import com.example.bootcamp.service.TvShowService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import java.util.List;

@RestController
@RequestMapping("/tv-shows")
public class TvShowController {

    private final CategoryService categoryService;
    private final TvShowService tvShowService;
    private final RestTemplate restTemplate;
    @Autowired
    public TvShowController(CategoryService categoryService, TvShowService tvShowService, RestTemplate restTemplate) {
        this.categoryService = categoryService;
        this.tvShowService = tvShowService;
        this.restTemplate = restTemplate;
    }
    @GetMapping
    public ResponseEntity<List<TvShow>> getAllTvShows() {
        List<TvShow> tvShows = tvShowService.findAll(Sort.by(Sort.Direction.DESC, "name"));
        return new ResponseEntity<>(tvShows, HttpStatus.OK);
    }
    @GetMapping("/")
    public ResponseEntity<List<TvShow>> getAllTvShowsSortedByNameReverse() {
        List<TvShow> tvShows = tvShowService.findAll(Sort.by(Sort.Direction.DESC, "name"));
        return new ResponseEntity<>(tvShows, HttpStatus.OK);
    }
    @GetMapping("/page")
    public ResponseEntity<Page<TvShow>> getAllTvShows(@RequestParam(defaultValue = "0") int page,
                                                      @RequestParam(defaultValue = "10") int size) {
        Pageable pageable = PageRequest.of(page, size);
        Page<TvShow> tvShowPage = (Page<TvShow>) tvShowService.getAllTvShows(pageable);
        return new ResponseEntity<>(tvShowPage, HttpStatus.OK);
    }
    @GetMapping("/action")
    public ResponseEntity<List<TvShow>> getActionTvShows() {
        List<TvShow> actionTvShows = tvShowService.getActionTvShows();
        return new ResponseEntity<>(actionTvShows, HttpStatus.OK);
    }
    @GetMapping("/filter")
    public ResponseEntity<List<TvShow>> filterTvShows(@RequestParam("edadMayorA") int edadMayorA,
                                                      @RequestParam("anio") int anio) {
        List<TvShow> filteredTvShows = tvShowService.filterTvShows(edadMayorA, anio);
        return new ResponseEntity<>(filteredTvShows, HttpStatus.OK);
    }
    @GetMapping("/{id}")
    public ResponseEntity<TvShow> getTvShowById(@PathVariable("id") int id) {
        // Obtener el TvShow por el id
        TvShow tvShow = tvShowService.findById(id).orElse(null);

        if (tvShow != null) {
            // Hacer la solicitud GET a la API FilmService
            String apiUrl = "http://localhost:8181/film-info/film/" + tvShow.getName();
            ResponseEntity<Film> response = restTemplate.getForEntity(apiUrl, Film.class);

            if (response.getStatusCode().is2xxSuccessful()) {
                Film film = response.getBody();
                if (film != null) {
                    double rate = film.getRate();
                    tvShow.setRate(rate);
                }
            }

            return new ResponseEntity<>(tvShow, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
    @PostMapping
    public ResponseEntity<TvShow> createTvShow(@RequestBody TvShow tvShow) {
        Category category = categoryService.findById(tvShow.getCategory().getId()).orElse(null);
        if (category != null) {
            tvShow.setCategory(category);
            TvShow createdTvShow = tvShowService.save(tvShow);
            return new ResponseEntity<>(createdTvShow, HttpStatus.CREATED);
        } else {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }
    @PutMapping("/{id}")
    public ResponseEntity<TvShow> updateTvShow(@PathVariable("id") Long id, @RequestBody TvShow tvShow) {
        TvShow existingTvShow = tvShowService.findById(Math.toIntExact(id)).orElse(null);
        if (existingTvShow != null) {
            Category category = categoryService.findById(tvShow.getCategory().getId()).orElse(null);
            if (category != null) {
                existingTvShow.setName(tvShow.getName());
                existingTvShow.setShortDescription(tvShow.getShortDescription());
                existingTvShow.setLongDescription(tvShow.getLongDescription());
                existingTvShow.setYear(tvShow.getYear());
                existingTvShow.setRecommendedAge(tvShow.getRecommendedAge());
                existingTvShow.setAdvertising(tvShow.getAdvertising());
                existingTvShow.setCategory(category);

                TvShow updatedTvShow = tvShowService.save(existingTvShow);
                return new ResponseEntity<>(updatedTvShow, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteTvShow(@PathVariable("id") int id) {
        TvShow existingTvShow = tvShowService.findById(Math.toIntExact(id)).orElse(null);
        if (existingTvShow != null) {
            tvShowService.delete(existingTvShow);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}








