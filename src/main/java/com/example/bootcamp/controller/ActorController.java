package com.example.bootcamp.controller;

import com.example.bootcamp.entity.Actor;
import com.example.bootcamp.service.ActorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping("/actors")
public class ActorController {
    private final ActorService actorService;

    @Autowired
    public ActorController(ActorService actorService) {

        this.actorService = actorService;
    }

    @GetMapping
    public ResponseEntity<List<Actor>> getAllActors() {
        List<Actor> actors = actorService.findAll();
        return new ResponseEntity<>(actors, HttpStatus.OK);
    }
    @GetMapping("/")
    public ResponseEntity<List<Actor>> getAllActorsSortedByName() {
        List<Actor> actors = actorService.getAllActorsSortedByName();
        return new ResponseEntity<>(actors, HttpStatus.OK);
    }
    @GetMapping("/page")
    public ResponseEntity<Page<Actor>> getAllActors(@RequestParam(defaultValue = "0") int page,
                                                    @RequestParam(defaultValue = "10") int size) {
        Pageable pageable = PageRequest.of(page, size);
        Page<Actor> actorPage = (Page<Actor>) actorService.getAllActors((java.awt.print.Pageable) pageable);
        return new ResponseEntity<>(actorPage, HttpStatus.OK);
    }
    @GetMapping("/prefix")
    public ResponseEntity<List<Actor>> getActorsByNamePrefix(@RequestParam String prefix) {
        List<Actor> actors = actorService.getActorsByNamePrefix(prefix);
        return new ResponseEntity<>(actors, HttpStatus.OK);
    }
    @PostMapping
    public ResponseEntity<Actor> createActor(@RequestBody Actor actor) {
        Actor createdActor = actorService.save(actor);
        return new ResponseEntity<>(createdActor, HttpStatus.CREATED);
    }
    @GetMapping("/{id}")
    public ResponseEntity<Actor> getActorById(@PathVariable("id") int id) {
        Actor actor = (Actor) actorService.findById(id).orElse(null);
        if (actor != null) {
            return new ResponseEntity<>(actor, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);

        }
    }
    @PutMapping("/{id}")
    public ResponseEntity<Actor> updateActor(@PathVariable("id") int id, @RequestBody Actor actor) {

        Actor updatedActor= actorService.updateActor(id, actor);
        if(updatedActor != null) {
            return new ResponseEntity<>(updatedActor, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteActor(@PathVariable("id") int id) {
        Actor existingActor = (Actor) actorService.findById(id).orElse(null);
        if (existingActor != null) {
            actorService.delete(existingActor);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}