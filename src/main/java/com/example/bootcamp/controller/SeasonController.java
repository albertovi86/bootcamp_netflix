package com.example.bootcamp.controller;

import com.example.bootcamp.entity.Season;
import com.example.bootcamp.entity.TvShow;
import com.example.bootcamp.service.SeasonService;
import com.example.bootcamp.service.TvShowService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping("/seasons")
public class SeasonController {

    private final TvShowService tvShowService;
    private final SeasonService seasonService;
    @Autowired
    public SeasonController(TvShowService tvShowService, SeasonService seasonService) {
        this.tvShowService = tvShowService;
        this.seasonService = seasonService;
    }
    @GetMapping
    public ResponseEntity<List<Season>> getAllSeasons() {
        List<Season> seasons = seasonService.findAll();
        return new ResponseEntity<>(seasons, HttpStatus.OK);
    }
    @GetMapping("/page")
    public ResponseEntity<Page<Season>> getAllSeasons(@RequestParam(defaultValue = "0") long page,
                                                      @RequestParam(defaultValue = "10") int size) {
        Pageable pageable = PageRequest.of((int) page, size);
        Page<Season> seasonPage = seasonService.getAllSeasons(pageable);
        return ResponseEntity.ok(seasonPage);
    }
    @GetMapping("/{id}")
    public ResponseEntity<Season> getSeasonById(@PathVariable("id") Long id) {
        Season season = seasonService.findById(Math.toIntExact(id)).orElse(null);
        if (season != null) {
            return new ResponseEntity<>(season, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
    @PostMapping
    public ResponseEntity<Season> createSeason(@RequestBody Season season) {
        TvShow tvShow = tvShowService.findById(season.getTvShow().getId()).orElse(null);
        if (tvShow != null) {
            season.setTvShow(tvShow);
            Season createdSeason = seasonService.save(season);
            return new ResponseEntity<>(createdSeason, HttpStatus.CREATED);
        } else {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }
    @PutMapping("/{id}")
    public ResponseEntity<Season> updateSeason(@PathVariable("id") Long id, @RequestBody Season season) {
        Season existingSeason = seasonService.findById(Math.toIntExact(id)).orElse(null);
        if (existingSeason != null) {
            TvShow tvShow = tvShowService.findById(season.getTvShow().getId()).orElse(null);
            if (tvShow != null) {
                existingSeason.setNumber(season.getNumber());
                existingSeason.setName(season.getName());
                existingSeason.setTvShow(tvShow);

                Season updatedSeason = seasonService.save(existingSeason);
                return new ResponseEntity<>(updatedSeason, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteSeason(@PathVariable("id") Long id) {
        Season existingSeason = seasonService.findById(Math.toIntExact(id)).orElse(null);
        if (existingSeason != null) {
            seasonService.delete(existingSeason);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}


