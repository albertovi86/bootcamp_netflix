package com.example.bootcamp.controller;


import com.example.bootcamp.entity.Chapter;
import com.example.bootcamp.entity.Season;
import com.example.bootcamp.service.ChapterService;
import com.example.bootcamp.service.SeasonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping("/chapters")
public class ChapterController {

    private final SeasonService seasonService;
    private final ChapterService chapterService;

    @Autowired
    public ChapterController(SeasonService seasonService, ChapterService chapterService) {
        this.seasonService = seasonService;
        this.chapterService = chapterService;
    }

    @GetMapping
    public ResponseEntity<List<Chapter>> getAllChapters() {
        List<Chapter> chapters = chapterService.findAll();
        return new ResponseEntity<>(chapters, HttpStatus.OK);
    }

    @GetMapping("/page")
    public ResponseEntity<Page<Chapter>> getAllChapters(@RequestParam(defaultValue = "0") int page,
                                                        @RequestParam(defaultValue = "10") int size) {
        Pageable pageable = PageRequest.of(page, size);
        Page<Chapter> chapterPage = chapterService.getAllChapters(pageable);
        return new ResponseEntity<>(chapterPage, HttpStatus.OK);
    }
    @GetMapping("/{id}")
    public ResponseEntity<Chapter> getChapterById(@PathVariable("id") int id) {
        Chapter chapter = chapterService.findById(id).orElse(null);
        if (chapter != null) {
            return new ResponseEntity<>(chapter, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
    @GetMapping("/{seasonId}/name/{suffix}")
    public ResponseEntity<List<Chapter>> getChaptersBySeasonAndNameEndingWith(
            @PathVariable("seasonId") int seasonId, @PathVariable("suffix") String suffix) {
        List<Chapter> chapters = chapterService.getChaptersBySeasonAndNameEndingWith(seasonId, suffix);
        return new ResponseEntity<>(chapters, HttpStatus.OK);
    }
    @PostMapping
    public ResponseEntity<Chapter> createChapter(@RequestBody Chapter chapter) {
        Season season = seasonService.findById(chapter.getSeason().getId()).orElse(null);
        if (season != null) {
            chapter.setSeason(season);
            Chapter createdChapter = chapterService.save(chapter);
            return new ResponseEntity<>(createdChapter, HttpStatus.CREATED);
        } else {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }
    @PutMapping("/{id}")
    public ResponseEntity<Chapter> updateChapter(@PathVariable("id") int id, @RequestBody Chapter chapter) {
        Chapter existingChapter = chapterService.findById(id).orElse(null);
        if (existingChapter != null) {
            Season season = seasonService.findById(chapter.getSeason().getId()).orElse(null);
            if (season != null) {
                existingChapter.setNumber(chapter.getNumber());
                existingChapter.setName(chapter.getName());
                existingChapter.setDuration(chapter.getDuration());
                existingChapter.setSeason(season);

                Chapter updatedChapter = chapterService.save(existingChapter);
                return new ResponseEntity<>(updatedChapter, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteChapter(@PathVariable("id") int id) {
        Chapter existingChapter = chapterService.findById(id).orElse(null);
        if (existingChapter != null) {
            chapterService.delete(existingChapter);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}


