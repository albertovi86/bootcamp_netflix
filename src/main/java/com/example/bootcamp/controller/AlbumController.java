package com.example.bootcamp.controller;

import com.example.bootcamp.entity.Album;
import com.example.bootcamp.service.AlbumService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;
@RestController
@RequestMapping("/albums")
public class AlbumController {
    private final AlbumService albumService;
    @Autowired
    public AlbumController(AlbumService albumService) {
        this.albumService = albumService;
    }
    @GetMapping
    public ResponseEntity<List<Album>> getAllAlbums() {
        List<Album> albums = albumService.getAllAlbums();
        return new ResponseEntity<>(albums, HttpStatus.OK);
    }
    @GetMapping("/sorted-by-year-release")
    public ResponseEntity<List<Album>> getAllAlbumsSortedByYearRelease() {
        List<Album> albums = albumService.getAllAlbumsSortedByYearRelease();
        return new ResponseEntity<>(albums, HttpStatus.OK);
    }
}


