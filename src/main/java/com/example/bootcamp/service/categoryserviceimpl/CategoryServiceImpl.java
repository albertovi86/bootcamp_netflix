package com.example.bootcamp.service.categoryserviceimpl;

import com.example.bootcamp.entity.Category;
import com.example.bootcamp.repository.CategoryRepository;
import com.example.bootcamp.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
@Service
public class CategoryServiceImpl implements CategoryService {
    private final CategoryRepository categoryRepository;
    @Autowired
    public CategoryServiceImpl(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }
    @Override
    public Page<Category> getAllCategories(Pageable pageable) {
        return categoryRepository.findAll(pageable);
    }
    @Override
    public Category getCategoryById(int id) {
        return categoryRepository.findById(Math.toIntExact(id)).orElse(null);
    }
    @Override
    public void deleteCategory(int id) {
        Category existingCategory = categoryRepository.findById(Math.toIntExact(id)).orElse(null);
        if (existingCategory != null) {
            categoryRepository.delete(existingCategory);
        }
    }
    @Override
    public Category createCategory(Category category) {
        return categoryRepository.save(category);
    }
    @Override
    public Category updateCategory(int id, Category category) {
        Category existingCategory = categoryRepository.findById(Math.toIntExact(id)).orElse(null);
        if (existingCategory != null) {
            existingCategory.setName(category.getName());
            return categoryRepository.save(existingCategory);
        }
        return null;
    }
    @Override
    public List<Category> findAll() {
        return categoryRepository.findAll();
    }
    @Override
    public Optional<Category> findById(int id) {
        return categoryRepository.findById(id);
    }
    @Override
    public Category save(Category category) {
        return categoryRepository.save(category);
    }
    @Override
    public void delete(Category existingCategory) {

    }
}

