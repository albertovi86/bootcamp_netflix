package com.example.bootcamp.service;

import com.example.bootcamp.entity.TvShow;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import java.util.List;
import java.util.Optional;
public interface TvShowService {
    Page<TvShow> getAllTvShows(Pageable pageable);
    TvShow getTvShowById(int id);
    void deleteTvShow(int id);
    TvShow getTvShowById(Long id);
    void deleteTvShow(Long id);
    TvShow createTvShow(TvShow tvShow);
    TvShow updateTvShow(int id, TvShow tvShow);
    TvShow addSeasonToTvShow(int tvShowId, int seasonId);
    void removeSeasonFromTvShow(int tvShowId, int seasonId);
    TvShow addCategoryToTvShow(int tvShowId, int categoryId);
    void removeCategoryFromTvShow(int tvShowId, int categoryId);
    TvShow updateTvShow(Long id, TvShow tvShow);
    TvShow addSeasonToTvShow(Long tvShowId, Long seasonId);
    void removeSeasonFromTvShow(Long tvShowId, Long seasonId);
    TvShow addCategoryToTvShow(Long tvShowId, Long categoryId);
    void removeCategoryFromTvShow(Long tvShowId, Long categoryId);
    List<TvShow> getAllTvShowsSortedByNameReverse();
    List<TvShow> getActionTvShows();
    List<TvShow> filterTvShows(int edadMayorA, int anio);
    List<TvShow> findAll(Sort name);
    Optional<TvShow> findById(int intExact);
    TvShow save(TvShow tvShow);
    void delete(TvShow existingTvShow);
}

