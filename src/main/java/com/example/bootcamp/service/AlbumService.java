package com.example.bootcamp.service;

import com.example.bootcamp.entity.Album;

import java.util.List;

public interface AlbumService {
    List<Album> getAllAlbumsSortedByYearRelease();
    List<Album> getAllAlbums();
}
