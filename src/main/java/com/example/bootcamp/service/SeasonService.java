package com.example.bootcamp.service;

import com.example.bootcamp.entity.Season;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import java.util.List;
import java.util.Optional;

public interface SeasonService {
    Page<Season> getAllSeasons(Pageable pageable);
    Season getSeasonById(int id);
    void deleteSeason(int id);
    Season getSeasonById(Long id);
    void deleteSeason(Long id);
    Season createSeason(Season season);
    Season updateSeason(int id, Season season);
    Season updateSeason(long id, Season season);
    Season addChapterToSeason(int seasonId, int chapterId);
    void removeChapterFromSeason(int seasonId, int chapterId);
    Season updateSeason(Long id, Season season);
    Season addChapterToSeason(Long seasonId, Long chapterId);
    void removeChapterFromSeason(Long seasonId, Long chapterId);
    List<Season> findAll();
    Optional<Season> findById(int intExact);
    Season save(Season season);
    void delete(Season existingSeason);
}


