package com.example.bootcamp.service.tvshowserviceimpl;

import com.example.bootcamp.entity.Category;
import com.example.bootcamp.entity.Season;
import com.example.bootcamp.entity.TvShow;
import com.example.bootcamp.repository.CategoryRepository;
import com.example.bootcamp.repository.SeasonRepository;
import com.example.bootcamp.repository.TvShowRepository;
import com.example.bootcamp.service.TvShowService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class TvShowServiceImpl implements TvShowService {
    private final TvShowRepository tvShowRepository;
    private final SeasonRepository seasonRepository;
    private final CategoryRepository categoryRepository;

    @Autowired
    public TvShowServiceImpl(TvShowRepository tvShowRepository, SeasonRepository seasonRepository, CategoryRepository categoryRepository) {
        this.tvShowRepository = tvShowRepository;
        this.seasonRepository = seasonRepository;
        this.categoryRepository = categoryRepository;
    }
    @Override
    public Page<TvShow> getAllTvShows(Pageable pageable) {
        return tvShowRepository.findAll(pageable);
    }
    @Override
    public TvShow getTvShowById(int id) {
        return null;
    }
    @Override
    public void deleteTvShow(int id) {

    }
    @Override
    public TvShow getTvShowById(Long id) {
        return tvShowRepository.findById(Math.toIntExact(id)).orElse(null);
    }
    @Override
    public void deleteTvShow(Long id) {
        TvShow existingTvShow = tvShowRepository.findById(Math.toIntExact(id)).orElse(null);
        if (existingTvShow != null) {
            tvShowRepository.delete(existingTvShow);
        }
    }
    @Override
    public TvShow createTvShow(TvShow tvShow) {
        return tvShowRepository.save(tvShow);
    }
    @Override
    public TvShow updateTvShow(int id, TvShow tvShow) {
        return null;
    }
    @Override
    public TvShow addSeasonToTvShow(int tvShowId, int seasonId) {
        return null;
    }
    @Override
    public void removeSeasonFromTvShow(int tvShowId, int seasonId) {

    }
    @Override
    public TvShow addCategoryToTvShow(int tvShowId, int categoryId) {
        return null;
    }
    @Override
    public void removeCategoryFromTvShow(int tvShowId, int categoryId) {

    }
    @Override
    public TvShow updateTvShow(Long id, TvShow tvShow) {
        TvShow existingTvShow = tvShowRepository.findById(Math.toIntExact(id)).orElse(null);
        if (existingTvShow != null) {
            existingTvShow.setName(tvShow.getName());
            existingTvShow.setShortDescription(tvShow.getShortDescription());
            existingTvShow.setLongDescription(tvShow.getLongDescription());
            existingTvShow.setYear(tvShow.getYear());
            existingTvShow.setRecommendedAge(tvShow.getRecommendedAge());
            existingTvShow.setAdvertising(tvShow.getAdvertising());
            return tvShowRepository.save(existingTvShow);
        }
        return null;
    }
    @Override
    public TvShow addSeasonToTvShow(Long tvShowId, Long seasonId) {
        TvShow existingTvShow = tvShowRepository.findById(Math.toIntExact(tvShowId)).orElse(null);
        Season existingSeason = seasonRepository.findById(Math.toIntExact(seasonId)).orElse(null);
        if (existingTvShow != null && existingSeason != null) {
            existingTvShow.addSeason(existingSeason);
            return tvShowRepository.save(existingTvShow);
        }
        return null;
    }
    @Override
    public void removeSeasonFromTvShow(Long tvShowId, Long seasonId) {
        TvShow existingTvShow = tvShowRepository.findById(Math.toIntExact(tvShowId)).orElse(null);
        Season existingSeason = seasonRepository.findById(Math.toIntExact(seasonId)).orElse(null);
        if (existingTvShow != null && existingSeason != null) {
            existingTvShow.removeSeason(existingSeason);
            tvShowRepository.save(existingTvShow);
        }
    }
    @Override
    public TvShow addCategoryToTvShow(Long tvShowId, Long categoryId) {
        TvShow existingTvShow = tvShowRepository.findById(Math.toIntExact(tvShowId)).orElse(null);
        Category existingCategory = categoryRepository.findById(Math.toIntExact(categoryId)).orElse(null);
        if (existingTvShow != null && existingCategory != null) {
            existingTvShow.addCategory(existingCategory);
            return tvShowRepository.save(existingTvShow);
        }
        return null;
    }
    @Override
    public void removeCategoryFromTvShow(Long tvShowId, Long categoryId) {
        TvShow existingTvShow = tvShowRepository.findById(Math.toIntExact(tvShowId)).orElse(null);
        Category existingCategory = categoryRepository.findById(Math.toIntExact(categoryId)).orElse(null);
        if (existingTvShow != null && existingCategory != null) {
            existingTvShow.removeCategory(existingCategory);
            tvShowRepository.save(existingTvShow);
        }
    }
    @Override
    public List<TvShow> getAllTvShowsSortedByNameReverse() {
        Sort sort = Sort.by(Sort.Direction.DESC, "name");
        return tvShowRepository.findAll(sort);
    }
    @Override
    public List<TvShow> getActionTvShows() {
        List<TvShow> allTvShows = tvShowRepository.findAll();
        List<TvShow> actionTvShows = new ArrayList<>();

        for (TvShow tvShow : allTvShows) {
            if (tvShow.getCategory().getName().equalsIgnoreCase("Accion")) {
                actionTvShows.add(tvShow);
            }
        }

        return actionTvShows;
    }
    @Override
    public List<TvShow> filterTvShows(int edadMayorA, int anio) {
        List<TvShow> allTvShows = tvShowRepository.findAll();
        List<TvShow> filteredTvShows = new ArrayList<>();

        for (TvShow tvShow : allTvShows) {
            if (tvShow.getRecommendedAge() >= edadMayorA && tvShow.getYear() == anio) {
                filteredTvShows.add(tvShow);
            }
        }

        return filteredTvShows;
    }
    @Override
    public List<TvShow> findAll(Sort name) {
        return tvShowRepository.findAll();
    }
    @Override
    public Optional<TvShow> findById(int id) {
        return tvShowRepository.findById(id);
    }
    @Override
    public TvShow save(TvShow tvShow) {
        return tvShowRepository.save(tvShow);
    }
    @Override
    public void delete(TvShow existingTvShow) {

    }

}

