package com.example.bootcamp.service.albumserviceimpl;

import com.example.bootcamp.entity.Album;
import com.example.bootcamp.repository.AlbumRepository;
import com.example.bootcamp.service.AlbumService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class AlbumServiceImpl implements AlbumService {
    private final AlbumRepository albumRepository;
    @Autowired
    public AlbumServiceImpl(AlbumRepository albumRepository) {
        this.albumRepository = albumRepository;
    }
    @Override
    public List<Album> getAllAlbumsSortedByYearRelease() {
        return albumRepository.findAllSortedByYearRelease();
    }
    @Override
    public List<Album> getAllAlbums() {
        return albumRepository.findAll();
    }
}


