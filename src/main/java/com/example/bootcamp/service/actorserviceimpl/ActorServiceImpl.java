package com.example.bootcamp.service.actorserviceimpl;

import com.example.bootcamp.entity.Actor;
import com.example.bootcamp.entity.Chapter;
import com.example.bootcamp.entity.TvShow;
import com.example.bootcamp.repository.ActorRepository;
import com.example.bootcamp.service.ActorService;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.awt.print.Pageable;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ActorServiceImpl implements ActorService {
    private final ActorRepository actorRepository;

    public ActorServiceImpl(ActorRepository actorRepository) {
        this.actorRepository = actorRepository;
    }

    @Override
    public List<Actor> getAllActors(Pageable pageable) {
        return actorRepository.findAll();
    }

    @Override
    public Actor getActorById(int id) {
        return actorRepository.findById(id).orElse(null);
    }
    @Override
    public Actor createActor(Actor actor) {
        return actorRepository.save(actor);
    }
    @Override
    public Actor updateActor(int id, Actor actor) {
        Actor existingActor = getActorById(id);
        if(existingActor == null) {
            return null;
        }
        existingActor.setName(actor.getName());
        existingActor.setDescription(actor.getDescription());
        return actorRepository.save(existingActor);
    }
    @Override
    public void deleteActor(int id) {
        Actor actor = getActorById((int) id);
        actorRepository.delete(actor);
    }
    @Override
    public List<TvShow> getActorTvShows(long actorId) {
        Actor actor = getActorById((int) actorId);
        return actor.getTvShows();
    }
    @Override
    public List<Chapter> getActorChapters(Long actorId) {
        Actor actor = getActorById(Math.toIntExact(actorId));
        List<Chapter> chapters = new ArrayList<>();
        for (TvShow tvShow : actor.getTvShows()) {
            chapters.addAll(tvShow.getChapters());
        }
        return chapters;
    }
    @Override
    public List<Actor> getAllActorsSortedByName() {
        return actorRepository.findAll(Sort.by(Sort.Direction.ASC, "name"));
    }
    @Override
    public List<Actor> getActorsByNamePrefix(String prefix) {
        return actorRepository.findAllByNameStartingWithIgnoreCase(prefix);
    }
    @Override
    public List<Actor> findAll() {
        return actorRepository.findAll();
    }
    @Override
    public void delete(Actor existingActor) {

    }
    @Override
    public Actor save(Actor actor) {
        return null;
    }
    @Override
    public Optional<Object> findById(int id) {
        return Optional.of(actorRepository.findById(id));
    }
}