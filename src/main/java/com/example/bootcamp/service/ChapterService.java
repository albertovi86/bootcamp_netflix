package com.example.bootcamp.service;

import com.example.bootcamp.entity.Chapter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import java.util.List;
import java.util.Optional;

public interface ChapterService {
    Page<Chapter> getAllChapters(Pageable pageable);
    Chapter getChapterById(int id);
    void deleteChapter(int id);
    Chapter createChapter(Chapter chapter);
    Chapter updateChapter(int id, Chapter chapter);
    Chapter addActorToChapter(int chapterId, int actorId);
    void removeActorFromChapter(int chapterId, int actorId);
    List<Chapter> getChaptersBySeasonAndNameSuffix(Long seasonId, String suffix);
    List<Chapter> findAll();
    Optional<Chapter> findById(int id);
    Chapter save(Chapter chapter);
    void delete(Chapter existingChapter);
    List<Chapter> getChaptersBySeasonAndNameEndingWith(int seasonId, String suffix);

}
