package com.example.bootcamp.service.chapterserviceimpl;

import com.example.bootcamp.entity.Actor;
import com.example.bootcamp.entity.Chapter;
import com.example.bootcamp.repository.ActorRepository;
import com.example.bootcamp.repository.ChapterRepository;
import com.example.bootcamp.service.ChapterService;
import com.example.bootcamp.service.SeasonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;

@Service
public class ChapterServiceImpl implements ChapterService {
    private final ChapterRepository chapterRepository;
    private final ActorRepository actorRepository;
    @Autowired
    public ChapterServiceImpl(SeasonService seasonService, ChapterRepository chapterRepository, ActorRepository actorRepository) {
        this.chapterRepository = chapterRepository;
        this.actorRepository = actorRepository;
    }
    @Override
    public Page<Chapter> getAllChapters(Pageable pageable) {
        return chapterRepository.findAll(pageable);
    }
    @Override
    public Chapter getChapterById(int id) {
        return chapterRepository.findById(id).orElse(null);
    }
    @Override
    public void deleteChapter(int id) {
        Chapter existingChapter = chapterRepository.findById(id).orElse(null);
        if (existingChapter != null) {
            chapterRepository.delete(existingChapter);
        }
    }
    @Override
    public Chapter createChapter(Chapter chapter) {
        return chapterRepository.save(chapter);
    }
    @Override
    public Chapter updateChapter(int id, Chapter chapter) {
        Chapter existingChapter = chapterRepository.findById(id).orElse(null);
        if (existingChapter != null) {
            existingChapter.setNumber(chapter.getNumber());
            existingChapter.setName(chapter.getName());
            existingChapter.setDuration(chapter.getDuration());
            return chapterRepository.save(existingChapter);
        }
        return null;
    }
    @Override
    public Chapter addActorToChapter(int chapterId, int actorId) {
        Chapter existingChapter = chapterRepository.findById(chapterId).orElse(null);
        Actor existingActor = actorRepository.findById(actorId).orElse(null);
        if (existingChapter != null && existingActor != null) {
            existingChapter.addActor(existingActor);
            return chapterRepository.save(existingChapter);
        }
        return null;
    }
    @Override
    public void removeActorFromChapter(int chapterId, int actorId) {
        Chapter existingChapter = chapterRepository.findById(chapterId).orElse(null);
        Actor existingActor = actorRepository.findById(actorId).orElse(null);
        if (existingChapter != null && existingActor != null) {
            existingChapter.removeActor(existingActor);
            chapterRepository.save(existingChapter);
        }
    }
    @Override
    public List<Chapter> getChaptersBySeasonAndNameSuffix(Long seasonId, String suffix) {
        return chapterRepository.findBySeasonIdAndNameEndingWith(seasonId, suffix);
    }
    @Override
    public List<Chapter> findAll() {
        return chapterRepository.findAll();
    }
    @Override
    public Optional<Chapter> findById(int id) {
        return chapterRepository.findById(id);
    }
    @Override
    public Chapter save(Chapter chapter) {
        return chapterRepository.save(chapter);
    }
    @Override
    public void delete(Chapter existingChapter) {

    }
    @Override
    public List<Chapter> getChaptersBySeasonAndNameEndingWith(int seasonId, String suffix) {
        List<Chapter> chapters = chapterRepository.findChaptersBySeasonAndNameEndingWith(seasonId, suffix);
        return chapters;
    }
}







