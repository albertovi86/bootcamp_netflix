package com.example.bootcamp.service;

import com.example.bootcamp.entity.Actor;
import com.example.bootcamp.entity.Chapter;
import com.example.bootcamp.entity.TvShow;
import java.awt.print.Pageable;
import java.util.List;
import java.util.Optional;
public interface ActorService {
    List<Actor> getAllActors(Pageable pageable);
    Actor getActorById(int id);
    Actor createActor(Actor actor);
    Actor updateActor(int id, Actor actor);
    void deleteActor(int id);
    List<TvShow> getActorTvShows(long actorId);
    List<Chapter> getActorChapters(Long actorId);
    List<Actor> getAllActorsSortedByName();
    List<Actor> getActorsByNamePrefix(String prefix);
    List<Actor> findAll();
    void delete(Actor existingActor);
    Actor save(Actor actor);
    Optional<Object> findById(int id);
}