package com.example.bootcamp.service.seasonserviceimpl;

import com.example.bootcamp.entity.Chapter;
import com.example.bootcamp.entity.Season;
import com.example.bootcamp.repository.ChapterRepository;
import com.example.bootcamp.repository.SeasonRepository;
import com.example.bootcamp.service.SeasonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;

@Service
public class SeasonServiceImpl implements SeasonService {
    private final SeasonRepository seasonRepository;
    private final ChapterRepository chapterRepository;
    private Pageable pageable;

    @Autowired
    public SeasonServiceImpl(SeasonRepository seasonRepository, ChapterRepository chapterRepository) {
        this.seasonRepository = seasonRepository;
        this.chapterRepository = chapterRepository;
    }
    @Override
    public Page<Season> getAllSeasons(Pageable pageable) {
        this.pageable = pageable;
        return seasonRepository.findAll(pageable);
    }
    @Override
    public Season getSeasonById(int id) {
        return seasonRepository.findById(Math.toIntExact(id)).orElse(null);
    }
    @Override
    public void deleteSeason(int id) {
        Season existingSeason = seasonRepository.findById(id).orElse(null);
        if (existingSeason != null) {
            seasonRepository.delete(existingSeason);
        }
    }
    @Override
    public Season getSeasonById(Long id) {
        return null;
    }
    @Override
    public void deleteSeason(Long id) {

    }
    @Override
    public Season createSeason(Season season) {
        return seasonRepository.save(season);
    }
    @Override
    public Season updateSeason(long id, Season season) {
        Season existingSeason = seasonRepository.findById((int) id).orElse(null);
        if (existingSeason != null) {
            existingSeason.setNumber(season.getNumber());
            existingSeason.setName(season.getName());
            return seasonRepository.save(existingSeason);
        }
        return null;
    }
    @Override
    public Season addChapterToSeason(int seasonId, int chapterId) {
        Season existingSeason = seasonRepository.findById(Math.toIntExact(seasonId)).orElse(null);
        Chapter existingChapter = chapterRepository.findById(chapterId).orElse(null);
        if (existingSeason != null && existingChapter != null) {
            existingSeason.addChapter(existingChapter);
            return seasonRepository.save(existingSeason);
        }
        return null;
    }
    @Override
    public void removeChapterFromSeason(int seasonId, int chapterId) {
        Season existingSeason = seasonRepository.findById(seasonId).orElse(null);
        Chapter existingChapter = chapterRepository.findById(chapterId).orElse(null);
        if (existingSeason != null && existingChapter != null) {
            existingSeason.removeChapter(existingChapter);
            seasonRepository.save(existingSeason);
        }
    }
    @Override
    public Season updateSeason(Long id, Season season) {
        return null;
    }
    @Override
    public Season updateSeason(int id, Season season) {
        return null;
    }
    @Override
    public Season addChapterToSeason(Long seasonId, Long chapterId) {
        return null;
    }
    @Override
    public void removeChapterFromSeason(Long seasonId, Long chapterId) {

    }
    @Override
    public List<Season> findAll() {
        return seasonRepository.findAll();
    }
    @Override
    public Optional<Season> findById(int id) {
        return seasonRepository.findById(id);
    }
    @Override
    public Season save(Season season) {
        return seasonRepository.save(season);
    }
    @Override
    public void delete(Season existingSeason) {

    }
    @Service
    public class SeasonService {
        private final SeasonRepository seasonRepository;
        @Autowired
        public SeasonService(SeasonRepository seasonRepository) {
            this.seasonRepository = seasonRepository;
        }
        public Page<Season> getAllSeasons(Pageable pageable) {
            return seasonRepository.findAll(pageable);
        }
    }
}


