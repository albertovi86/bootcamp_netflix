package com.example.bootcamp.service;

import com.example.bootcamp.entity.Category;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;
public interface CategoryService {
    Page<Category> getAllCategories(Pageable pageable);
    Category getCategoryById(int id);
    void deleteCategory(int id);
    Category createCategory(Category category);
    Category updateCategory(int id, Category category);
    List<Category> findAll();
    Optional<Category> findById(int id);
    Category save(Category category);
    void delete(Category existingCategory);
}
